const fetch = require('node-fetch');

let testData = null;
let testService = false;

/* Test service */
setInterval(() => {
    if (testService) return;
    testService = true;
    /* Request */
    fetch('http://127.0.0.1:3051', {
        headers: {
            accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            testService = false;
            if (json.data !== testData) {
                testData = json.data;
                console.log(`SUCCESS: ${testData}`);
            }
        })
        .catch(e => {
            if (testData !== 'ERROR') {
                testData = 'ERROR';
                console.log('ERROR');
            }
            testService = false;
        });
}, 1);
