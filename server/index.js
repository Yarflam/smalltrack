const http = require('http');
const path = require('path');
const fs = require('fs');

const HOST = '0.0.0.0';
const PORT = 3051;
const TIME_SAVE = 5 * 1000; // save them every 5 seconds

/* Gestion des dates */
function getDate(d = new Date()) {
    return {
        day: `0${d.getDate()}`.slice(-2),
        month: `0${d.getMonth()}`.slice(-2),
        year: String(d.getFullYear())
        // hour: `0${d.getHours()}`.slice(-2),
        // minute: `0${d.getMinutes()}`.slice(-2),
        // seconds: `0${d.getSeconds()}`.slice(-2)
    };
}

/* Capture the logs */
let logs = [];
const capture = () => {
    /* Name */
    const date = getDate();
    const fname = `smalltrack-${date.year}${date.month}${date.day}.log`;
    const fpath = path.resolve(__dirname, 'logs', fname);
    /* No change */
    const check = fs.existsSync(fpath);
    if (!logs.length && check) {
        setTimeout(() => capture(), TIME_SAVE);
        return;
    }
    /* Open, Add and Save */
    let data = check ? fs.readFileSync(fpath, 'utf8') : '';
    if (logs.length) {
        data += logs.splice(0, logs.length).join('\n') + '\n';
    }
    fs.writeFileSync(fpath, data, 'utf8');
    /* Next */
    setTimeout(() => capture(), TIME_SAVE);
};
capture();

/* Webserver */
const server = http.createServer((req, res) => {
    /* Save */
    if (/^\/\?d=.+$/.test(req.url)) {
        let data = decodeURIComponent(req.url.slice(4));
        logs.push(`${new Date().toISOString()} ${data}`);
    }
    /* Basic content + CORS */
    res.statusCode = 200;
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Accept, ontent-Type, Content-Length'
    );
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    res.setHeader(
        'Cache-Control',
        'private, no-cache, no-store, must-revalidate'
    );
    res.setHeader('Content-Type', 'application/json');
    res.write(
        JSON.stringify({
            success: true,
            error: false,
            data: 'OK'
        })
    );
    res.end();
});

//
(async () => {
    /* Waiting */
    console.log('Restart 3s');
    await new Promise(resolve => {
        setTimeout(() => resolve(), 3000);
    });
    /* Listen */
    server.listen(PORT, HOST, () => {
        console.log(`Server listen on ${HOST}:${PORT}`);
    });
})();
