#!/bin/bash
docker-compose -f ./deploy/docker-compose.yml build
docker push 127.0.0.1:5000/smalltrack

docker stack deploy -c ./deploy/docker-compose.yml --with-registry-auth smalltrack
